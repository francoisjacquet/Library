Library Premium module
======================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_screenshot.png)

FEATURES
--------

- Set a Color code for each document Category.
- Document Fields: add custom fields (type, ISBN, collection, editor, language, state, etc.)
- Limit document Category Visibility to selected User Profiles and Grade Levels.
- Upload and attach file (PDF, epub, MP3, etc).
- Search documents globally by Reference, Title, Author and Year.
- Return Comment.
- Quick Loan: quickly search and lend a document to the selected user or student.
- Portal and automatic email notifications when documents are late / past due date.
- Admin Dashboard integration.
- Loans Breakdown by month and category.
- Automate loans.

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_screenshot-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_screenshot.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_screenshot_2-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_screenshot_2.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_screenshot_3-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_screenshot_3.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_screenshot_4-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_screenshot_4.png)

### [➭ Switch to Premium](https://www.rosariosis.org/modules/library/#premium-module)
