��          �      �           	  	             -     6     J     \  	   s     }     �     �     �     �     �     �     �     �     �  	   �     �     �       �  &               "     7  
   @  
   K     V  	   n     x          �     �  
   �     �     �     �     �  
   �  	   �     �                                                                                 
      	                           Author Available Borrowed documents Document Document Categories Document Category Document was returned. Documents Has late documents Lend Lend "%s" to %s Lent Library Loan Loans New Document New Document Category Past Due Reference Return Date Return document The document was lent. Project-Id-Version: Library module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:14+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Autor Na voljo Izposojeni dokumenti Dokument Kategorije Kategorija Dokument je bil vrnjen. Dokumenti Zamuja Izposoja Posodite "%s" osebi  "%s" Izposoja Knjižnica Izposoja Izposoje Nov dokument Nova kategorija Preteklost Referenca Vrnjeno dne Vrni dokument Dokument je bil posojen. 