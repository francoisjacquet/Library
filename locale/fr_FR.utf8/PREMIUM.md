Module Bibliothèque Premium
===========================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_fr_screenshot.png)

CARACTÉRISTIQUES
----------------

- Attribuer une couleur à chaque catégorie de documents.
- Champs document : ajouter des champs personnalisés (type, ISBN, collection, éditeur, langue, état, etc.)
- Limiter la visibilité de la catégorie de documents aux profils utilisateur et aux niveaux scolaires sélectionnés.
- Joindre un fichier (PDF, epub, MP3, etc).
- Chercher vos documents de manière globale : référence, titre, auteur et année.
- Commentaire de retour.
- Prêt rapide : cherchez et prêtez rapidement un document à l’utilisateur ou l’élève sélectionné.
- Alerte du portail et notification automatique par email lorsque vos documents sont en retard / passé la date de retour.
- Intégration au Tableau de bord admin.
- Répartition des prêts par mois et catégorie.
- Automatiser les prêts.

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_fr_screenshot-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_fr_screenshot.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_fr_screenshot_2-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_fr_screenshot_2.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_fr_screenshot_3-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_fr_screenshot_3.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_fr_screenshot_4-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_fr_screenshot_4.png)

### [➭ Passer à Premium](https://www.rosariosis.org/fr/modules/library/#premium-module)
