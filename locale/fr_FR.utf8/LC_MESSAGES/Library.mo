��          �   %   �      `     a  	   h     r     �     �     �     �     �  	   �     �     �     �                    *     /     5     E     R     h  	   q     {     �     �  +   �  �  �     �  
   �     �     �     �          &     6  	   U     _     y     �     �     �     �     �     �     �     �     �  	        "     .     =     S  .   l                                               
                                 	                                            Author Available Borrowed documents Document Document Categories Document Category Document Fields Document was returned. Documents Has late documents Lend Lend "%s" to %s Lent Library Library Premium Loan Loans Loans Breakdown New Document New Document Category Past Due Reference Return Date Return document The document was lent. This program is available in the %s module. Project-Id-Version: Library module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-01-07 15:47+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Auteur Disponible Documents empruntés Document Catégories de document Catégorie de document Champs document Le document a été retourné. Documents A des documents en retard Prêter Prêter "%s" à "%s" Prêté Bibliothèque Bibliothèque Premium Prêt Prêts Répartition des prêts Nouveau document Nouvelle catégorie de document En retard Référence Date de retour Retourner le document Le document est prêté. Ce programme est disponible dans le module %s. 