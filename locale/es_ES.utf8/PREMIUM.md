Módulo Biblioteca Premium
=========================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_es_screenshot.png)

CARACTERÍSTICAS
---------------

- Atribuir un color a cada categoría de documentos.
- Campos Documento: agregar campos personalizados (tipo, ISBN, colección, editor, lenguaje, estado, etc.)
- Limitar la visibilidad de una Categoría de Documentos a los Perfiles de Usuario y los Grados seleccionados.
- Adjuntar un archivo (PDF, epub, MP3, etc.).
- Buscar sus documentos de manera global: Referencia, Título, Autor y Año.
- Comentario de Vuelta.
- Préstamo rápido: busque y preste rápidamente un documento al usuario o estudiante seleccionado.
- Alerta en el Portal y notificaciones automáticas por email cuando sus documentos están retrasados / pasaron la fecha de devolución.
- Integración al Tablero de Mandos admin.
- Repartición de los préstamos por mes y categoría.
- Automatizar los prestamos.

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_es_screenshot-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_es_screenshot.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_es_screenshot_2-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_es_screenshot_2.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_es_screenshot_3-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_es_screenshot_3.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_es_screenshot_4-300.png)](https://www.rosariosis.org/wp-content/uploads/2019/03/rosariosis_library_premium_es_screenshot_4.png)

### [➭ Cambiar a Premium](https://www.rosariosis.org/es/modules/library/#premium-module)
