��          �   %   �      `     a  	   h     r     �     �     �     �     �  	   �     �     �     �                    *     /     5     E     R     h  	   q     {     �     �  +   �    �     �  
   �     �  	             0     I     Z  
   u     �     �     �     �  
   �     �     �  	   �     �     
          9  
   B     M     b     x  0   �                                               
                                 	                                            Author Available Borrowed documents Document Document Categories Document Category Document Fields Document was returned. Documents Has late documents Lend Lend "%s" to %s Lent Library Library Premium Loan Loans Loans Breakdown New Document New Document Category Past Due Reference Return Date Return document The document was lent. This program is available in the %s module. Project-Id-Version: Library module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-01-07 15:46+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Autor Disponible Documentos prestados Documento Categorías de Documentos Categoría de Documentos Campos Documento Se devolvió el documento. Documentos Tiene documentos con retraso Prestar Prestar "%s" a "%s" Prestado Biblioteca Biblioteca Premium Prestamo Prestamos Análisis de Prestamos Nuevo Documento Nueva Categoría de Documentos Aplazado Referencia Fecha de Devolución Devolver el documento El documento está prestado. Este programa está disponible en el módulo %s. 