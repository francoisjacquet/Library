��          �      �           	  	             -     6     J     \  	   s     }     �     �     �     �     �     �     �     �     �  	   �     �     �       �  &               *  	   A     K     d     {  
   �     �  	   �     �  
   �  
   �     �                    9     G     S     c     v                                                                       
      	                           Author Available Borrowed documents Document Document Categories Document Category Document was returned. Documents Has late documents Lend Lend "%s" to %s Lent Library Loan Loans New Document New Document Category Past Due Reference Return Date Return document The document was lent. Project-Id-Version: Library module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:14+0200
Last-Translator: Emerson Barros
Language-Team: RosarioSIS <info@rosariosis.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Autor Disponível Documentos emprestados Documento Categorias de documentos Categoria do documento O documento foi devolvido. Documentos Existem documentos atrasados Emprestar Emprestar "%s" para %s Emprestado Biblioteca Empréstimo Empréstimos Novo documento Nova categoria de documento Fora do prazo Referência Data de retorno Devolver documento O documento foi emprestado. 